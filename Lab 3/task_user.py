""" @file        task_user.py
    @brief       User interface task for a singular encoder.
    @details     Implements a finite state machine to recieve inputs from
                 the keyboard and execute the corresponding task.
                 
                 Lab 2 task user finite-state machine.
                 \image html Lab2_Task_User_FSM.PNG
                 
                 Lab 3 task user finite-state machine.
                 \image html Lab3_Task_User_FSM.jpg
                 
    @author      Corey Agena
    @author      Austin Fong
    @date        October 14, 2021
"""

# All imports near the top
import utime, pyb
from micropython import const

## State 0 of the user interface task
S0_INIT             = const(0)
## State 1 of the user interface task
S1_WAIT_FOR_CHAR    = const(1)
## State 2 of the user interface task
S2_COLLECT          = const(2)
## State 3 of the user interface task
S3_PRINT            = const(3)
## State 4 of the user interface task
S4_MOTOR            = const(4)

class Task_User:
    ''' @brief      User class for the singular encoder interface task.
        @details    Implements a class that can be used for other applications.
    '''
            
    def __init__(self, name, period, ZeroFlag_1, position_1, delta_1, 
                 my_queue_pos1, my_queue_del1, duty_1, ZeroFlag_2, position_2, 
                 delta_2, my_queue_pos2, my_queue_del2, duty_2, FaultFlag,
                 ClearFaultFlag, dbg=False):
        ''' @brief                  Constructs a user task.
            @details                The user task is implemented as a finite
                                    state machine.
            @param name             The name of the task
            @param period           The period, in milliseconds, between runs
                                    of the task.
            @param ZeroFlag_1       A shares.Share object used to zero
                                    encoder 1.
            @param position_1       A shares.Share object used to report
                                    position of encoder 1.
            @param delta_1          A shares.Share object used to report delta
                                    of encoder 1.
            @param my_queue_pos1    A shares.Queue object used to store 
                                    encoder 1 position and time to be printed
                                    later.
            @param my_queue_del1    A shares.Queue object used to store
                                    encoder 1 delta values and time to be
                                    printed later.
            @param duty_1           A shares.Share object used to set the
                                    dutycycle of motor 1.
            @param ZeroFlag_2       A shares.Share object used to zero
                                    encoder 2.
            @param position_2       A shares.Share object used to report
                                    position of encoder 2.
            @param delta_2          A shares.Share object used to report delta
                                    of encoder 2.
            @param my_queue_pos2    A shares.Queue object used to store 
                                    encoder 2 position and time to be printed
                                    later.
            @param my_queue_del2    A shares.Queue object used to store
                                    encoder 2 delta values and time to be
                                    printed later.
            @param duty_2           A shares.Share object used to set the
                                    dutycycle of motor 2.     
            @param FaultFlag        Flag to indicate if the fault condition is
                                    active or not.
            @param ClearFaultFlag   Flag to clear the fault condition and
                                    enable the motor.
            @param dbg              A boolean flag used to enable or disable 
                                    debug messages printed over the VCP.            
        '''     
        ## The name of the task
        self.name = name
        ## The period (in us) of the task
        self.period = period
        
        ## shares.Share objects used to zero the encoders
        self.ZeroFlag_1 = ZeroFlag_1
        self.ZeroFlag_2 = ZeroFlag_2
        ## shares.Share objects for position
        self.position_1 = position_1
        self.position_2 = position_2
        ## shares.Share objects for delta
        self.delta_1 = delta_1
        self.delta_2 = delta_2
        ## shares.Queue objects to store position data
        self.queue_pos1 = my_queue_pos1
        self.queue_pos2 = my_queue_pos2
        
        ## shares.Queue objects to store delta data
        self.queue_del1 = my_queue_del1
        self.queue_del2 = my_queue_del2
        
        ## shares.Share objects for duty cycle
        self.duty_1 = duty_1
        self.duty_2 = duty_2
        
        ## shares.Share object used to stop the motors if there is a fault
        self.FaultFlag = FaultFlag
        
        ## shares.Share object used to clear the fault and enable the motor
        self.ClearFaultFlag = ClearFaultFlag
        
        ## A flag indicating if debugging print messages display
        self.dbg = dbg
        
        ## A serial port to use for user I/O
        self.ser = pyb.USB_VCP()
        
        ## The state to run on the next iteration of the finite state machine
        self.state = S0_INIT
        ## The number of runs of the state machine
        self.runs = 0
        
        ## The variable used to report time
        self.n = 0
        
        ## Set the next_time variable for the first iteration of the code
        self.next_time = self.period
        
        ## Set the initial time to zero
        self.initial_time = 0
        
        ## Create a encoder flag and set it to zero
        self.enc_flag = 0
        
        ## Create a motor flag and set it to zero
        self.mot_flag = 0
        
        ## Create a empty string object
        self.num_str = ''
        
    def run(self):
        ''' @brief         Runs one iteration of the task.
            @details       Waits for a command and executes based on 
                           the typed letter.
        '''
        current_time = utime.ticks_ms()
        if (utime.ticks_diff(current_time, self.initial_time) >=
            self.next_time):
            if self.runs == 0:
                self.initial_time = utime.ticks_ms()
            elif self.state == S0_INIT:
                print('Welcome, press:'
                      '\n\'z\' to zero the position of encoder 1'
                      '\n\'Z\' to zero the position of encoder 2'
                      '\n\'p\' to print the position of encoder 1'
                      '\n\'P\' to print the position of encoder 2'
                      '\n\'d\' to print out the delta for encoder 1'
                      '\n\'D\' to print out the delta for encoder 2'
                      '\n\'m\' to prompt the user to enter a duty cycle for'
                      'motor 1'
                      '\n\'M\' to prompt the user to enter a duty cycle for'
                      'motor 2'
                      '\n\'c\' to clear a fault condition triggered by DRV8847'
                      '\n\'g\' to collect encoder 1 data for 30 seconds'
                      'and print it'
                      '\n\'G\' to collect encoder 2 data for 30 seconds'
                      'and print it'
                      '\n\'s\' to end data collection prematurely'
                      '\n\'h\' return to the welcome screen')
                self.transition_to(S1_WAIT_FOR_CHAR)
                
            elif self.state == S1_WAIT_FOR_CHAR:
                if self.ser.any():
                    char_in = self.ser.read(1).decode()
                    if(char_in == 'z'):
                        self.ZeroFlag_1.write(True)
                        print('Position of Encoder 1 is Zeroed')
                    elif(char_in == 'Z'):
                        self.ZeroFlag_2.write(True)
                        print('Position of Encoder 2 is Zeroed')
                    elif(char_in == 'p'):
                        print(self.position_1.read())
                    elif(char_in == 'P'):
                        print(self.position_2.read())
                    elif(char_in == 'd'):
                        print(self.delta_1.read())
                    elif(char_in == 'D'):
                        print(self.delta_2.read())
                    elif(char_in == 'm'):
                        print('Please enter the desired duty cycle'
                              ' for motor 1:')
                        self.mot_flag = 0
                        self.transition_to(S4_MOTOR)
                    elif(char_in == 'M'):
                        print('Please enter the desired duty cycle'
                              ' for motor 2:')
                        self.mot_flag = 1
                        self.transition_to(S4_MOTOR)
                    elif(char_in == 'c' or char_in == 'C'):
                        self.FaultFlag.write(False)
                        self.ClearFaultFlag.write(True)
                        print("Fault Condition Cleared")
                    elif(char_in == 'g'):
                        self.transition_to(S2_COLLECT)
                        self.runs = 0
                        self.enc_flag = 0
                    elif(char_in == 'G'):
                        self.transition_to(S2_COLLECT)
                        self.runs = 0
                        self.enc_flag = 1
                    elif(char_in == 'h' or char_in == 'H'):
                        self.transition_to(S0_INIT)
                    else:
                        print('Command \'{:}\' is invalid.'.format(char_in))
                        
            elif self.state == S2_COLLECT:
                ## Create a start time variable.
                if self.runs == 1:
                    self.stime = utime.ticks_ms()
                    print('Begin Data Collection')
                    
                ## Collect timer values when the g command is used.
                self.gtime = utime.ticks_ms()
                
                if self.gtime - self.stime <= 30_000:
                    if self.enc_flag == 0:
                        self.queue_pos1.put(self.position_1.read())
                        self.queue_del1.put(self.delta_1.read())
                    elif self.enc_flag == 1:
                        self.queue_pos2.put(self.position_2.read())
                        self.queue_del2.put(self.delta_2.read())
                    if self.ser.any():
                        char_in = self.ser.read(1).decode()
                        if(char_in == 's' or char_in == 'S' or 
                           self.FaultFlag == True):
                            self.transition_to(S3_PRINT)
                            self.duty_1.write(0)
                            self.duty_2.write(0)  
                else:
                    self.transition_to(S3_PRINT)
                    
            elif self.state == S3_PRINT:
                if self.queue_pos1.num_in() > 0:
                    print("{:},{:},{:}".format(round(self.n, 1),
                                               round(self.queue_pos1.get(), 2),
                                               round(self.queue_del1.get(), 2)))
                                              
                    self.n += self.period/1000
                elif self.queue_pos2.num_in() > 0:
                    print("{:},{:},{:}".format(round(self.n, 1),
                                               round(self.queue_pos2.get(), 2),
                                               round(self.queue_del2.get(), 2)))
                    self.n += self.period/1000
                else:
                    self.transition_to(S1_WAIT_FOR_CHAR)
                    self.n = 0
                    self.ZeroFlag_1.write(True)
                    self.ZeroFlag_2.write(True)
                    
            elif self.state == S4_MOTOR:
                if self.ser.any():
                    char_in = self.ser.read(1).decode()
                    if char_in.isdigit():
                        self.ser.write(char_in)
                        self.num_str += char_in
                    elif char_in == '-':
                        if self.num_str == '':
                            self.ser.write(char_in)
                            self.num_str += char_in
                    elif char_in == '\b' or char_in == '\x7F':
                        self.ser.write('\x7F')
                        if self.num_str != '':
                            self.num_str = self.num_str[:-1]
                    elif char_in == '.':
                        if '.' not in self.num_str:
                            self.ser.write(char_in)
                            self.num_str += char_in
                    elif char_in == '\r' or char_in == '\n':
                        self.ser.write('\r')
                        if self.num_str != '':
                            char_f = float(self.num_str)
                            if char_f <= 100 and char_f >= -100:
                                if self.mot_flag == 0:
                                    self.duty_1.write(char_f)
                                    print('Duty cycle for motor 1 is'
                                          ' set to {:}'.format(self.num_str))
                                elif self.mot_flag == 1:
                                    self.duty_2.write(char_f)
                                    print('Duty cycle for motor 2 is'
                                          ' set to {:}'.format(self.num_str))
                                self.num_str = ''
                                self.transition_to(S1_WAIT_FOR_CHAR)
                                print('Enter a new command, pressing h will'
                                      ' return you to the welcome screen.')
                            else:
                                print('Duty cycle must be between -100 and'
                                      ' 100\nPlease enter a duty cycle:')
                                self.num_str = ''
                        else:
                            print('No duty cycle provided,'
                                  'please enter a duty cycle:')
            else:
                raise ValueError('Invalid State')
            
            self.next_time += self.period
            self.runs += 1
            
    def transition_to(self, new_state):
        ''' @brief      Transitions the FSM to a new state.
            @details    Optionally a debugging message can be printed
                        if the dbg flag is set when the task object is created.
            @param      new_state The state to transition to.
        '''
        if (self.dbg):
            print('{:}: S{:}->S{:}'.format(self.name,self.state,new_state))
        self.state = new_state