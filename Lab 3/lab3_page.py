''' @file                lab3_page.py
    @brief               File that holds the Lab 3 documentation.
    @details             The Lab 3 documentation including the task diagram
                         and finite state machine.
    
    @page page1          Lab 3 Documentation
    
    @subsection plots    Plots
                         Motor position spun in the negative direction.
                         \image html pos1.jpg
                
                         Motor angular velocity spun in the negative direction.
                         \image html speed1.jpg
                
                         Motor position spun in the positive direction.
                         \image html pos2.jpg
                
                         Motor angular velocity spun in the positive direction.
                         \image html speed2.jpg

    @subsection task     Task Diagram
                         \image html Lab3_Task_Diagram.jpg
                         
    @subsection encoder  Encoder Task Finite State Machine
                         \image html Lab2_Task_Encoder_FSM.PNG

    @subsection motor    Motor Task Finite State Machine
                         \image html Lab3_Task_Motor_FSM.jpg
                         
    @subsection user     User Task Finite State Machine
                         \image html Lab3_Task_User_FSM.jpg                
'''