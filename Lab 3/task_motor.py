""" @file        task_motor.py
    @brief       Motor task for running the motor.
    @details     Runs the motor at the provided duty cycle.
                 Task motor finite-state machine:
                 \image html Lab3_Task_Motor_FSM.jpg
                 
    @author      Corey Agena
    @author      Austin Fong
    @date        October 24, 2021
"""

class Task_Motor:
    ''' @brief      Motor task class for running the motor.
        @details    Runs the motor at the provided duty cycle
    '''
    def __init__(self, motor_drv, duty, mot, FaultFlag):    
        ''' @brief              Constructs motor task objects.
            @details            The motor task is implemented as a finite 
                                state machine.
            @param motor_drv    The motor driver for the system.
            @param duty         The duty cycle of the motor.
            @param mot          The motor object that is affected.
            @param FaultFlag    Flag to indicate if the fault condition is
                                active or not.
        '''
        ## Create a motor driver object
        self.motor_drv = motor_drv       
        
        ## The intended duty cycle of the motor
        self.duty = duty
        
        ## The motor object that will be affected.
        self.mot = mot
        
        ## The flag that will stop the motor if a fault is detected.
        self.FaultFlag = FaultFlag
    
    def run(self):
        ''' @brief         Runs one iteration of the task.
            @details       It sets the duty cycle of the motor.
        '''
        if self.FaultFlag.read() == False:
            if self.duty != 0:
                self.mot.set_duty(self.duty.read())
        else:
            self.mot.set_duty(0)