##  @file                    lab1.py
#   @brief                   Cycles through various LED light patterns with the 
#                            press of a button
#   @details                 See video demonstration: 
#                            https://www.youtube.com/watch?v=DZUZjOI8bSA.
<<<<<<< HEAD
#   @image html              lab1diagram.PNG                     
=======
#   @image html              lab1diagram.png                     
>>>>>>> bf757a6 (Lab 2 Commit 10/19)
#   @author                  Austin Fong
#   @author                  Corey Agena
#   @date                    October 4, 2021


# All imports should go at the top
import pyb
import math
import utime

## @brief         Detects when the button is pressed
# @param IRQ-src  Interrupt handler for callback function.
def onButtonPressFCN(IRQ_src):
    global ButtonFlag
    ButtonFlag = True
    
## @brief          Sets the global variable "startTime" to the current timer value.
def reset_timer(): 
    ## @brief       Variable is set to the current timer value and corresponds
    #               to the start of a pattern phase.
    global startTime
    startTime = utime.ticks_ms()

## @brief          Computes the elapsed time.
#  @return         Returns the difference between the start and stop time.
def update_timer():    
    ## @brief      Variable is set to the current timer value.
    stopTime = utime.ticks_ms()
    
    ## @brief       Variable is set to the difference between the start and stop
    #               time and corresponds to the relative time spent in the current
    #               pattern phase.
    current_time = utime.ticks_diff(stopTime, startTime)
    return current_time

## @brief              Updates the brightness of the LED in the square wave pattern.
#                      If the modulus of the current time is less than 0.5, return
#                      false. Return true if greater than 0.5.
#  @param current_time The elapsed time from the start of the square wave 
#                      pattern phase.
#  @return             Returns true or false based on the modulus of the current
#                      time.
def update_sqw(current_time):
    
    return 100*((current_time/1000) % 1.0 < 0.5)

## @brief               Updates the brightness of the LED in the sine wave pattern.
#                       This calculates the amplitude of the sine wave at the given
#                       current time.
#  @param current_time  The elapsed time from the start of the sine wave 
#                       pattern phase.
#  @return              Returns the amplitude of the sine wave.
def update_sw(current_time):
    return 50 * math.sin(math.pi * ((current_time)/5000)) + 50

## @brief               Updates the brightness of the LED in the sawtooth wave 
#                       pattern. The modulus resets the returned value back to 
#                       0 whenever the value reaches 100.
#  @param current_time  The elapsed time from the start of the sawtooth wave 
#                       pattern phase.
#  @return              Returns the amplitude of the sawtooth wave.
def update_stw(current_time):
    return 100*((current_time/1000) % 1.0)

# The main program should go at the bottom after function definitions.
if __name__ == '__main__':
    ## @brief The state the finite state machine is about to run
    state = 0
    ## @brief The number of iterations of the finite state machine
    runs = 0
    ## @brief Create a pin object for the button, PC13
    pinC13 = pyb.Pin(pyb.Pin.cpu.C13)
    ## @brief Create a pin object for the LED, A5
    pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
    ## @brief Configure I/O pins to interrupt on button press
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                           pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)
    ## @brief Variable is set to True whenever the button is pressed
    ButtonFlag = False
    
    # Set the brightness of the the green LED
    tim2 = pyb.Timer(2, freq=20000)
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)

    while True:
        try:                        # If no keyboard interrrupt, run FSM
            if state == 0:
                # run state 0 code
                # Show welcome message on start up
                print(
                    'Press the user button, B1 on the Nucleo, to cycle through LED patterns')
                # Immediately transition to state 1
                state = 1           # Transition to state 1

            elif state == 1:
                # run state 1 code
                # Run the following code if the button has been pressed
                if ButtonFlag == True:
                    state = 2       # Transition to state 2
                    print('Square pattern selected')
                    ButtonFlag = False
                    reset_timer()

            elif state == 2:
                # run state 2 code
                # Run the following code if the button has been pressed
                if ButtonFlag == True:
                    state = 3       # Transitions to state 3
                    print('Sine pattern selected')
                    ButtonFlag = False
                    reset_timer()
                # Run the following code if the button is not being pressed
                else:
                # Update the brightness of the LED
                    current_time = update_timer()
                    t2ch1.pulse_width_percent(update_sqw(current_time))

            elif state == 3:
                # run state 3 code
                # Run the following code if the button has been pressed
                if ButtonFlag == True:
                    state = 4       # Transitions to state 4
                    print('Saw pattern selected')
                    ButtonFlag = False
                    reset_timer()
                # Run the following code if the button is not being pressed
                else:
                # Update the brightness of the LED
                    current_time = update_timer()
                    t2ch1.pulse_width_percent(update_sw(current_time))

            elif state == 4:
                # run state 4 code
                # Run the following code if the button has been pressed
                if ButtonFlag == True:
                    state = 2       # Transitions to state 2
                    print('Square pattern selected')
                    ButtonFlag = False
                    reset_timer()
                # Run the following code if the button is not being pressed
                else:
                # Update the brightness of the LED
                    current_time = update_timer()
                    t2ch1.pulse_width_percent(update_stw(current_time))

            runs += 1               # Increment run counter to track number
            # of FSM iterations

        except KeyboardInterrupt:   # If keyboard interrupt, exit loop
            break

    print('Program Terminating')
    # Turn off the LED light
    pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
    pinA5.low()
