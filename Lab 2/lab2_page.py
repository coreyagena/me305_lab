''' @file                lab2_page.py
    @brief               File that holds the Lab 2 documentation.
    @details             The Lab 2 documentation including the task diagram
                         and finite state machine.
    
    @page page1          Lab 2 Documentation
    
    @subsection task     Task Diagram
                         \image html Lab2_Task_Diagram.PNG
                         
    @subsection encoder  Encoder Task Finite State Machine
                         \image html Lab2_Task_Encoder_FSM.PNG
                         
    @subsection user     User Task Finite State Machine
                         \image html Lab2_Task_User_FSM.PNG                     
'''