''' @file       main.py
    @brief      Main script for cooperative multitasking example.
    @details    Implements cooperative multitasking using tasks implemented by
                finite state machines.
                
                \image html Lab2_Task_Diagram.png
                
    @author     Corey Agena
    @author     Austin Fong
    @date       October 5, 2021
'''

import shares
import task_user
import task_encoder

def main():
    ''' @brief            The main program to run both tasks.
        @details          Creates share objects and runs the user and encoder
                          tasks.
    '''
    
    ## Create a shares.Share object to reset the encoder.
    ZeroFlag = shares.Share()
    
    ## Create a shares.Share object for position.
    position = shares.Share()
    
    ## Create a shares.Share object for delta.
    delta = shares.Share()
    
    ## Create a shares.Queue object.
    my_queue = shares.Queue()
    
    ## A user interface object
    task1 = task_user.Task_User('user', 10, ZeroFlag, position, 
                                delta, my_queue)
    ## A encoder task object
    task2 = task_encoder.Task_Encoder(1, ZeroFlag, position, delta)
    
    ## A list of tasks to run
    task_list = [task1, task2]
    
    while(True):
        try:
            for task in task_list:
                task.run()
            
        except KeyboardInterrupt:
            break
    
    print('Program Terminating')

if __name__ == '__main__':
    main()
