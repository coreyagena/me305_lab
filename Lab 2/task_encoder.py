""" @file        task_encoder.py
    @brief       Encoder task for updating encoder interface.
    @details     Implements a finite state machine to continuosly update the
                 encoder position and zero the encoder when required.
                 
                 \image html Lab2_Task_Encoder_FSM.png
                 
    @author      Corey Agena
    @author      Austin Fong
    @date        October 5, 2021
"""
## All imports near the top
import encoder
import utime

class Task_Encoder:
    ''' @brief      Encoder task class for updating encoder position.
        @details    Implements a finite state machine
    '''
    def __init__(self, period, ZeroFlag, position, delta):    
        ''' @brief              Constructs a user task.
            @details            The user task is implemented as a finite state
                                machine.
            @param name         The name of the task
            @param period       The period, in milliseconds, between runs of 
                                the task.
            @param ZeroFlag     A shares.Share object used to zero the encoder.
            @param position     A shares.Share object used to report position.
            @param delta        A shares.Share object used to report delta.
        '''
        ## The period (in us) of the task
        self.period = period
        ## A shares.Share object used to zero the encoder
        self.ZeroFlag = ZeroFlag
        ## A shares.Share object for position
        self.position = position
        ## A shares.Share object for delta
        self.delta = delta
        ## An encoder object
        self.enc = encoder.Encoder('B6', 'B7', 4 )
        
    def run(self):
        ''' @brief         Runs one iteration of the task.
            @details       It zeros the encoder if required and updates
                           position otherwise.
        '''
        self.current = utime.ticks_ms()
        if self.current >= self.period:
            if self.ZeroFlag.read() == True:
                self.enc.set_position(0)
                self.enc.update()
                self.ZeroFlag.write(False)
                self.position.write(self.enc.get_position())
            else:
                self.enc.update()
                self.position.write(self.enc.get_position())
                self.delta.write(self.enc.get_delta())
            self.current = utime.ticks_add(self.current,self.period*-1)