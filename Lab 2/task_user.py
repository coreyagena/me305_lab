""" @file        task_user.py
    @brief       User interface task for a singular encoder.
    @details     Implements a finite state machine to recieve inputs from
                 the keyboard and execute the corresponding task.
                 
                 \image html Lab2_Task_User_FSM.png
                 
    @author      Corey Agena
    @author      Austin Fong
    @date        October 14, 2021
"""
# All imports near the top
import utime, pyb
from micropython import const

## State 0 of the user interface task
S0_INIT             = const(0)
## State 1 of the user interface task
S1_WAIT_FOR_CHAR    = const(1)
## State 2 of the user interface task
S2_COLLECT          = const(2)
## State 3 of the user interface task
S3_PRINT            = const(3)

class Task_User:
    ''' @brief      User class for the singular encoder interface task.
        @details    Implements a class that can be used for other applications.
    '''
            
    def __init__(self, name, period, ZeroFlag, position, delta, 
                 my_queue, dbg=False):
        ''' @brief              Constructs a user task.
            @details            The user task is implemented as a finite state
                                machine.
            @param name         The name of the task
            @param period       The period, in milliseconds, between runs of 
                                the task.
            @param ZeroFlag     A shares.Share object used to zero the encoder.
            @param position     A shares.Share object used to report position.
            @param delta        A shares.Share object used to report delta.
            @param my_queue     A shares.Queue object used to store encoder
                                position and time to be printed later.
            @param dbg          A boolean flag used to enable or disable debug
                                messages printed over the VCP.            
        '''     
        ## The name of the task
        self.name = name
        ## The period (in us) of the task
        self.period = period
        ## A shares.Share object used to zero the encoder
        self.ZeroFlag = ZeroFlag
        ## A shares.Share object for position
        self.position = position
        ## A shares.Share object for delta
        self.delta = delta
        
        ## A shares.Queue object to store data
        self.queue = my_queue
        
        ## A flag indicating if debugging print messages display
        self.dbg = dbg
        
        ## A serial port to use for user I/O
        self.ser = pyb.USB_VCP()
        
        ## The state to run on the next iteration of the finite state machine
        self.state = S0_INIT
        ## The number of runs of the state machine
        self.runs = 0
        
        ## The variable used to report time
        self.n = 0
        
        ## Set the next_time variable for the first iteration of the code
        self.next_time = self.period
        
        ## Set the initial time to zero
        self.initial_time = 0
        
    def run(self):
        ''' @brief         Runs one iteration of the task.
            @details       Waits for a command and executes based on 
                           the typed letter.
        '''
        current_time = utime.ticks_ms()
        if (utime.ticks_diff(current_time, self.initial_time) >= self.next_time):
            if self.runs == 0:
                self.initial_time = utime.ticks_ms()
            if self.state == S0_INIT:
                print('Welcome, press:'
                      '\n\'z\' to zero the position of the encoder'
                      '\n\'p\' to print the position of encoder 1'
                      '\n\'d\' to print out the delta for encoder 1'
                      '\n\'g\' to collect encoder 1 data for 30 seconds'
                      'and print it'
                      '\n\'s\' end data collection prematurely'
                      '\n\'h\' return to the welcome screen')
                self.transition_to(S1_WAIT_FOR_CHAR)
                
            elif self.state == S1_WAIT_FOR_CHAR:
                if self.ser.any():
                    char_in = self.ser.read(1).decode()
                    if(char_in == 'z' or char_in == 'Z'):
                        self.ZeroFlag.write(True)
                        print('Position Zeroed')
                    elif(char_in == 'p' or char_in == 'P'):
                        print(self.position.read())
                    elif(char_in == 'd' or char_in == 'D'):
                        print(self.delta.read())
                    elif(char_in == 'g' or char_in == 'G'):
                        self.transition_to(S2_COLLECT)
                        self.runs = 0
                    elif(char_in == 'h' or char_in == 'H'):
                        self.transition_to(S0_INIT)
                    else:
                        print('Command \'{:}\' is invalid.'.format(char_in))
                        
            elif self.state == S2_COLLECT:
                ## Create a start time variable.
                if self.runs == 1:
                    self.stime = utime.ticks_ms()
                    print('Begin Data Collection')
                    
                ## Collect timer values when the g command is used.
                self.gtime = utime.ticks_ms()
                
                if self.gtime - self.stime <= 30_000:
                    self.queue.put(self.position.read())
                    if self.ser.any():
                        char_in = self.ser.read(1).decode()
                        if(char_in == 's' or char_in == 'S'):
                            self.transition_to(S3_PRINT)  
                else:
                    self.transition_to(S3_PRINT)
                        
            elif self.state == S3_PRINT:
                if self.queue.num_in() > 0:
                    print("{:},{:}".format(round(self.n, 2),self.queue.get()))
                    self.n += 0.01
                else:
                    self.transition_to(S1_WAIT_FOR_CHAR)
            else:
                raise ValueError('Invalid State')
            
            self.next_time += self.period
            self.runs += 1
            
    def transition_to(self, new_state):
        ''' @brief      Transitions the FSM to a new state.
            @details    Optionally a debugging message can be printed
                        if the dbg flag is set when the task object is created.
            @param      new_state The state to transition to.
        '''
        if (self.dbg):
            print('{:}: S{:}->S{:}'.format(self.name,self.state,new_state))
        self.state = new_state
            