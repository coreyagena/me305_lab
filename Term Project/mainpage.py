''' @file                mainpage.py
    @brief               File that holds the main page.
    @details             The main page includes sections with links to other
                         parts of the bitbucket portfolio.

    @mainpage

    @subsection intro    Introduction
                         Lab assignments from the Introduction to Mechatronics
                         ME 305 class at Cal Poly. The report for the term
                         project can be found at
                         <A HREF = "https://coreyagena.bitbucket.io/page2.html">
                         <B>Term Project Report</B></A>. The
                         source code can be accessed here: 
                         <A HREF = "https://bitbucket.org/coreyagena/me305_lab/src/master/Term%20Project/">
                         <B>Source Code</B></A>.
                         
    @subsection main     Main File
                         Main script for cooperative multitasking. Please see
                         main.py for details.
                        
    @subsection mot      Motor Driver
                         The motor driver includes a class for the DRV8847
                         device and a class for one channel of the DRV8847.
                         Please see DRV8847.DRV8847 and DRV8847.Motor for 
                         details.
                        
    @subsection cont     Controller Driver
                         A driver for controlling the angular velocity of the
                         motor. Please see controller.Controller for 
                         details.

    @subsection IMU      IMU Driver
                         A driver for the IMU with functions to get
                         calibration data, set calibration values, get euler
                         angles, and get angular velocities of the panel.
                         Please see IMU.IMU for details.
    
    @subsection panel    Panel Driver
                         A driver for panel that updates the position and
                         velocity of the ball. Please see
                         touch_panel.Touch_Panel for details.
                         
    @subsection user     User Task
                         User interface task for the motors, controllers, IMU,
                         and touch panel. Please see task_user.Task_User for
                         details.
                         
    @subsection mot_t    Motor Task
                         The motor task implements a finite state machine to
                         run the motor and control for the system. Please see
                         task_motor.Task_Motor for details.
                         
    @subsection IMU_t    IMU Task
                         The IMU task implements a finite state machine to
                         calibrate the IMU and read euler angles and angular
                         velocities from the IMU. Please see
                         task_IMU.Task_IMU for details.

    @subsection panel_t  Panel Task
                         The panel task implements a finite state machine to
                         run calibrate the panel and read position and
                         velocity from the touch panel. Please see
                         task_panel.Task_Panel for details.                         
    
    @subsection model    Ball Balancer System Modeling
                         A kinematic and kinetic analysis of the Ball
                         Balancing System. Please see
                         <A HREF = "Ball_Balancer_System_Modeling.html">
                         <B>Ball_Balancer_System_Modeling.html</B></A>
    
    @subsection source   Documentation and Source Code
                         The documentation, diagrams, and source code
                         for each lab will be linked below.
                         
                         <U>Lab 1</U>
                         \n <A HREF = "https://coreyagena.bitbucket.io/Lab1/index.html">
                         <B>Doxygen Documentation</B></A>
                         \n <A HREF = "https://coreyagena.bitbucket.io/Lab1/page1.html">
                         <B>Finite State Machine</B></A>
                         \n <A HREF = "https://bitbucket.org/coreyagena/me305_lab/src/master/Lab%201/">
                         <B>Source Code</B></A>
                         
                         <U>Lab 2</U>
                         \n <A HREF = "https://coreyagena.bitbucket.io/Lab2/page1.html">
                         <B>Documentation</B></A>
                         \n <A HREF = "https://bitbucket.org/coreyagena/me305_lab/src/master/Lab%202/">
                         <B>Source Code</B></A>
                         
                         <U>Lab 3</U>
                         \n <A HREF = "https://coreyagena.bitbucket.io/Lab3/page1.html">
                         <B>Documentation</B></A>
                         \n <A HREF = "https://bitbucket.org/coreyagena/me305_lab/src/master/Lab%203/">
                         <B>Source Code</B></A>
                         
                         <U>Lab 4</U>
                         \n <A HREF = "https://coreyagena.bitbucket.io/Lab4/page1.html">
                         <B>Documentation</B></A>
                         \n <A HREF = "https://bitbucket.org/coreyagena/me305_lab/src/master/Lab%204/">
                         <B>Source Code</B></A>
                         
                         <U>Term Project</U>
                         Term Project Report
                         \n <A HREF = "https://bitbucket.org/coreyagena/me305_lab/src/master/Term%20Project/">
                         <B>Source Code</B></A>
                         
    @author              Corey Agena

    @date                Fall 2021
'''