""" @file        task_motor.py
    @brief       Motor task for running the motor.
    @details     Implements a finite state machine to run the motor at the
                 duty cycle provided by the user or the controller.
    @author      Corey Agena
    @author      Austin Fong
    @date        October 24, 2021
"""
import utime

class Task_Motor:
    ''' @brief      Motor task class for running the motor.
        @details    Runs the motor at the provided duty cycle.
                    Task Motor Finite State Machine.
                    \image html Final_Task_Motor_FSM.jpg
    '''
    def __init__(self, period, motor_drv, duty, mot, cont, state_vector,
                 b_flag, dis_flag):    
        ''' @brief              Constructs motor task objects.
            @details            The motor task is implemented as a finite 
                                state machine.
            @param period       The period used to run the motor task.
            @param motor_drv    The motor driver of the system.
            @param duty         The duty cycle of the motor.
            @param mot          The motor object that is affected.
            @param cont         The controller object.
            @param state_vector The state vector representing the data read
                                from the touch panel and the IMU.
            @param b_flag       Flag to indicate the controller state.
            @param dis_flag     Flag to set the duty cycle of both motors to
                                zero, effectively disabling the motors.
        '''
        ## The period (in us) of the task.
        self.period = period
        
        ## Create a motor driver object.
        self.motor_drv = motor_drv       
        
        ## The intended duty cycle of the motor.
        self.duty = duty
        
        ## The motor object that will be affected.
        self.mot = mot
        
        ## The controller object that will be affected.
        self.cont = cont
        
        ## A shares.Share object that represents the state vector.
        self.state_vector = state_vector
        
        ## The flag that will change the state of the controller.
        self.b_flag = b_flag
        
        ## The flag to disable the motor.
        self.dis_flag = dis_flag
        
        ## Set the next_time variable for the first iteration of the code.
        self.next_time = self.period
        
        ## Set the initial time to zero.
        self.initial_time = 0
        
        ## The number of runs of the state machine.
        self.runs = 0
    
    def run(self):
        ''' @brief         Runs one iteration of the task.
            @details       It sets the duty cycle of the motor.
        '''
        current_time = utime.ticks_us()
        if (utime.ticks_diff(current_time, self.initial_time) >=
            self.next_time):
            if self.runs == 0:
                self.initial_time = utime.ticks_us()
            elif self.dis_flag.read() == False:
                if self.b_flag.read() == 0:
                    self.mot.set_duty(self.duty.read())
                elif self.b_flag.read() == 1 or self.b_flag.read() == 2:
                    self.cont.update()
                    self.mot.set_duty(self.duty.read())
            else:
                self.mot.set_duty(0)
            self.next_time += self.period
            self.runs += 1