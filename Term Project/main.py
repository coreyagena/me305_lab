''' @file       main.py
    @brief      Main script for cooperative multitasking.
    @details    Implements cooperative multitasking using tasks implemented by
                finite state machines.
                
                Task diagram
                \image html Final_Task_Diagram.jpg
                
    @author     Corey Agena
    @author     Austin Fong
    @date       October 5, 2021
'''

import shares
import task_user
import DRV8847
import task_motor
import controller
import touch_panel
import task_panel
import IMU
import task_IMU
import pyb
from ulab import numpy as np

def main():
    ''' @brief            The main program to run both tasks.
        @details          Creates share objects and runs the user and encoder
                          tasks.
    '''
    
    ## Create shares.Share objects for the duty cycles of both motors.
    duty_1 = shares.Share(0)
    duty_2 = shares.Share(0)
    
    ## Create shares.Share objects for the proportional gain of each
    ## controller.
    Kp_init_1 = np.array([-0.006,-.05,30,0.0005])
    Kp_init_2 = np.array([0.0055,.04,30,-0.0005])
    
    ## Create a shares.Share object for the controller
    b_flag = shares.Share(0)
    
    ## Create a shares.Share object for the motor.
    dis_flag = shares.Share(False)
    
    ## modify the code to facilitate passing in the pins and timer objects
    ## needed to run the motors.
    motor_drv = DRV8847.DRV8847(3)
    motor_1 = motor_drv.motor('B4','B5',1,2)
    motor_2 = motor_drv.motor('B0','B1',3,4)
    
    ## Create a shares.Share objects for the state vectors.
    state_vect1 = (shares.Share(0),shares.Share(0),shares.Share(0),shares.Share(0))
    state_vect2 = (shares.Share(0),shares.Share(0),shares.Share(0),shares.Share(0))
    
    ## Create a controller driver object for each motor.
    controller_1 = controller.Controller(Kp_init_1, state_vect1,
                                         -72, 72, duty_1)
    controller_2 = controller.Controller(Kp_init_2, state_vect2,
                                         -72, 72, duty_2)
    
    ## Create a shares.Share object for a calibration flag.
    calib_flag_tp = shares.Share(0)
    calib_flag_IMU = shares.Share(0)
    
    ## Create a shares.Share objects for calibration values.
    calib_values_tp = shares.Share()
    calib_values_IMU = shares.Share()

    ## Create a panel driver object for the touch panel.
    panel_drv = touch_panel.Touch_Panel(calib_flag_tp, calib_values_tp)

    ## Create a IMU driver object for the IMU.
    i2c = pyb.I2C(1, pyb.I2C.MASTER)
    IMU_drv = IMU.IMU(i2c)
    
    ## A user task object for the period of the task in us.
    userPeriod = 20_000
    
    ## A user interface object for both encoders
    task_1 = task_user.Task_User('user', userPeriod, duty_1, duty_2, b_flag,
                                 dis_flag, calib_flag_tp, calib_flag_IMU,
                                 state_vect1, state_vect2)
    
    ## A motor task object for the period of the task in us.
    motorPeriod = 10_000
    
    ## A motor task object for the first motor
    task_2 = task_motor.Task_Motor(motorPeriod, motor_drv, duty_1, motor_1,
                                   controller_1, state_vect1, b_flag,
                                   dis_flag)
    
    ## A motor task object for the second motor
    task_3 = task_motor.Task_Motor(motorPeriod, motor_drv, duty_2, motor_2,
                                   controller_2, state_vect2, b_flag,
                                   dis_flag)
    
    ## A panel task object for the period of the task in us.
    panelPeriod = 5_000
    
    ## A panel task object for the touch panel
    task_4 = task_panel.Task_Panel(panelPeriod, panel_drv, calib_flag_tp,
                                   calib_values_tp, state_vect1, state_vect2,
                                   b_flag)
    
    ## A IMU task object for the period of the task in us.
    IMUPeriod = 10_000
    
    ## A IMU task object for the IMU
    task_5 = task_IMU.Task_IMU(IMUPeriod, IMU_drv, i2c, calib_flag_IMU,
                               calib_values_IMU, state_vect1, state_vect2)
    
    ## A list of tasks to run
    task_list = [task_1, task_2, task_3, task_4, task_5]
    
    while(True):
        try:
            for task in task_list:
                task.run()
                
        except KeyboardInterrupt:
            break
    
    print('Program Terminating')

if __name__ == '__main__':
    main()
