''' @file       touch_panel.py
    @brief      The touch panel driver.
    @details    The touch panel driver reads data from the resistive touch
                panel and returns it to be used in the touch panel task.
    @author     Corey Agena
    @author     Austin Fong
    @date       November 16, 2021
'''
from pyb import Pin
from pyb import ADC
import utime
import micropython
from ulab import numpy as np

## Create Constants for the pin configurations
OUT_PP = micropython.const(Pin.OUT_PP)
IN     = micropython.const(Pin.IN)

class Touch_Panel:
    ''' @brief          A touch panel driver class for the resistive touch
                        panel.
        @details        Objects of this class can be used to configure the
                        touch panel driver and to create one or more objects
                        of the Touch Panel class which can be used to perform
                        touch panel tasks.
    '''
    def __init__(self, calib_flag, cal_values):
        ''' @brief          Initializes and returns a Touch_Panel object.
            @details        Initializes variables to be used in other 
                            fucntions.
            @param          calib_flag Flag to show if the touch panel is
                            calibrated.
            @param          cal_values Calibration values of the touch panel.
        '''
        
        ## shares.Share object for the calibration flag
        self.calib_flag = calib_flag
        
        ## shares.Share object for the calibration values of the touch panel.
        self.cal_values = cal_values
        
        ## Initialize variables for the ADC readings.
        self.x_ADC = 0
        self.y_ADC = 0
        self.z_ADC = 0
        
        ## Initialize variables for the readings converted to the proper units.
        self.x = 0
        self.y = 0
        self.z = 0
        
        ## The coordinates used to calibrate the touch panel.
        self.coord = np.array([[-80,-40],[80,-40],[80,40],[-80,40],[0,0]])
        
        ## Initialize a list for the ADC values used for calibration.
        self.arr_list = ["ADC0","ADC1","ADC2","ADC3","ADC4"]
        
        ## Initialize a variable for the number of runs.
        self.runs = 0
        
        ## Initialize a variable for the number of runs of the calibration.
        self.runs_calib = 0
        
        ## Initialize a array of zeros for the ADC data.
        self.ADC_array = np.zeros((5,3))
        
        ## Initialize variables that will be used to calculate the velocity.
        self.x_prev = 0
        self.y_prev = 0
        self.t_prev = 0
        self.t_c = 0
        
    def update(self):
        ''' @brief          Performs x, y, and z scans.
            @details        Performs the x, y, and z scans and documents the
                            position. Then calculates the velocities and
                            documents that as well.
            @return         A vector with x and y, position and velocity.
        '''
        ## Get data from the cal_values share.
        self.cal_array = self.cal_values.read()
        
        ## X Scan
        pin_ym = Pin(Pin.cpu.A0, IN)
        pin_xm = Pin(Pin.cpu.A1, OUT_PP)
        pin_yp = Pin(Pin.cpu.A6, IN)
        pin_xp = Pin(Pin.cpu.A7, OUT_PP)
        pin_xp.high()
        pin_xm.low()
        x_ADC = ADC(pin_ym)
        self.x_ADC = (x_ADC.read())
        
        ## Y Scan
        pin_ym = Pin(Pin.cpu.A0, OUT_PP)
        pin_xm = Pin(Pin.cpu.A1, IN)
        pin_yp = Pin(Pin.cpu.A6, OUT_PP)
        pin_xp = Pin(Pin.cpu.A7, IN)
        pin_yp.high()
        pin_ym.low()
        y_ADC = ADC(pin_xm)
        self.y_ADC = (y_ADC.read())
            
        self.runs += 1
        
        ## Z Scan
        pin_ym = Pin(Pin.cpu.A0, IN)
        pin_xm = Pin(Pin.cpu.A1, OUT_PP)
        pin_yp = Pin(Pin.cpu.A6, OUT_PP)
        pin_xp = Pin(Pin.cpu.A7, IN)
        pin_yp.high()
        pin_xm.low()
        self.z_ADC = ADC(pin_xp)
        if self.z_ADC.read() >= 50 and self.z_ADC.read() <= 5000:
            self.z = True
        else:
            self.z = False
            self.x = 0
            self.y = 0
            self.x_vel = 0
            self.y_vel = 0
        
        if self.calib_flag.read() >= 1 and self.z == True:
            if self.runs == 0:
                self.x_prev = self.cal_array[0]*self.x_ADC + self.cal_array[1]*self.y_ADC + self.cal_array[4]
                self.y_prev = self.cal_array[2]*self.x_ADC + self.cal_array[3]*self.y_ADC + self.cal_array[5]
                self.t_prev = utime.ticks_us()
                self.runs = 1
                pass
            t_c = utime.ticks_us()
            self.dt = utime.ticks_diff(t_c, self.t_prev)
            self.x = self.cal_array[0]*self.x_ADC + self.cal_array[1]*self.y_ADC + self.cal_array[4]
            self.y = self.cal_array[2]*self.x_ADC + self.cal_array[3]*self.y_ADC + self.cal_array[5]
            self.x_vel = (self.x - self.x_prev)/self.dt
            self.y_vel = (self.y - self.y_prev)/self.dt
            
            self.x_prev = self.x
            self.y_prev = self.y
            self.t_prev = self.t_c
            
        ## Return
        pos_vel = [self.x, self.y, self.x_vel, self.y_vel]
        return pos_vel
                       
    def report(self):
        ''' @brief          Reports the positions and velocities
            @details        Prints the positions and velocities to the REPL.
        '''
        utime.sleep(.2)
        scan = (self.x, self.y, self.x_vel, self.y_vel)
        print(scan)
    
    def calibrate(self):
        ''' @brief          Calibrates the touch panel.
            @details        Runs through a calibration process that has the
                            user touch the panel in various locations. Then
                            creates a vector with the data and computes the
                            vector of calibration coefficients.
            @return         A vector of the calibration coefficients.
        '''
        for x in self.coord:
            print('Touch the panel at the coordinates, {:}.'
                  .format(self.coord[self.runs_calib, :]))
            while True:
                utime.sleep(.4)
                self.update()
                if self.z == True:
                    self.ADC_array[self.runs_calib,0] = self.x_ADC
                    print(self.x_ADC)
                    self.ADC_array[self.runs_calib,1] = self.y_ADC
                    print(self.y_ADC)
                    self.ADC_array[self.runs_calib,2] = 1
                    break
            self.runs_calib += 1

        self.ADC_transpose = self.ADC_array.transpose()
        self.ADC_s1 = np.dot(self.ADC_transpose,self.ADC_array)
        self.ADC_s2 = np.linalg.inv(self.ADC_s1)
        self.ADC_s3 = np.dot(self.ADC_transpose,self.coord)
        self.beta = np.dot(self.ADC_s2,self.ADC_s3)
        self.beta_a = np.concatenate((self.beta[0], self.beta[1], self.beta[2]))
        return self.beta_a
        self.runs = 0
        
            
if __name__ == '__main__':
    
    touch_panel = Touch_Panel()
    
    while True:
        utime.sleep(0.5)
        start_time = utime.ticks_us()
        touch_panel.update()
        time = utime.ticks_diff(utime.ticks_us(),start_time)
        ## print(time)
        touch_panel.report()