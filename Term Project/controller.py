""" @file        controller.py
    @brief       A driver for controlling the angular velocity of the motor.
    @details     The driver updates continuously and outputs actuation values.
    @author      Corey Agena
    @author      Austin Fong
    @date        October 26, 2021
"""
from ulab import numpy as np

class Controller:
    ''' @brief             Control angular velocity of the motors.
        @details           The Controller class can be called and used with
                           different hardware requiring closed loop control.
    '''
    def __init__(self, Kp_init, state_vect, pwm_sat_low,
                 pwm_sat_high, duty):
        ''' @brief                Constructs a controller object
            @details              The initialization creates objects
                                  required for control.
            @param Kp_init        The initial proportional gain provided by
                                  the user.
            @param state_vect     The state vector representing the data in
                                  from the touch panel and IMU.
            @param pwm_sat_low    Lower Saturation limit on the PWM level for
                                  the current hardware.
            @param pwm_sat_high   Upper Saturation limit on the PWM level for
                                  the current hardware.
            @param duty           A shares.Share object for the duty cycle
                                  provided to the motor after control is
                                  complete.
        '''
        ## Create a proportional gain variable and set it to the initial value.
        self.Kp = Kp_init
        
        ## Create a reference angular velocity variable.
        #self.omega_ref = omega_ref
        
        ## Create a measured angular velocity variable.
        #self.omega_meas = omega_meas
        
        ## Create a measured state vector variable.
        self.state_vect = state_vect
        self.state_array = np.zeros((4))
        
        ## Create PWM saturation variables.
        self.pwm_sat_low = pwm_sat_low
        self.pwm_sat_high = pwm_sat_high
        
        ## Create a actuation variable and set it to zero.
        self.L = 0
        
        ## Create a share.Shares object for the duty cycle.
        self.duty = duty
        
        ## Create a variable for the gain from torque to gain.
        self.gain = (100*2.21*1000)/(4*13.8*12)
        
    def update(self):
        ''' @brief          Calculates the actuation value.
            @details        Computes and returns the actuation value based on
                            the measured and reference values.
        '''
        self.state_array[0] = self.state_vect[0].read()
        self.state_array[1] = self.state_vect[1].read()
        self.state_array[2] = self.state_vect[2].read()
        self.state_array[3] = self.state_vect[3].read()
        if self.state_vect[1].read() < 0:
            #self.L = self.Kp.read()*abs(self.omega_ref.read() - self.omega_meas.read())*(-1)
            self.L = np.dot(-self.Kp,self.state_array) * self.gain
        elif self.state_vect[1].read() >= 0:
            #self.L = self.Kp.read()*abs(self.omega_ref.read() - self.omega_meas.read())
            self.L = np.dot(-self.Kp,self.state_array) * self.gain
        if self.L > self.pwm_sat_high:
            self.L = self.pwm_sat_high
        elif self.L < self.pwm_sat_low:
            self.L = self.pwm_sat_low
        elif self.L >= -25 and self.L < 0:
            self.L = -25
        elif self.L <= 25 and self.L > 0:
            self.L = 25
        self.duty.write(self.L)
            
    def get_Kp(self):
        ''' @brief          Returns the proportional gain.
            @return         The proportional gain.
        '''
        return self.Kp
    
    def set_Kp(self,Kp):
        ''' @brief          Changes the proportional gain.
        '''
        self.Kp = Kp