""" @file        IMU.py
    @brief       A driver to read and write IMU information.
    @details     The driver updates continuously and includes methods to
                 complete important tasks.
    @author      Corey Agena
    @author      Austin Fong
    @date        November 2, 2021
"""
## All imports near the top
import pyb
import time
import struct
import os

class IMU:
    ''' @brief             Interface with the IMU.
        @details           The BNO055 class can be called and used with
                           different IMUs.
    '''
    def __init__(self, i2c):
        ''' @brief                Constructs objects to use in the IMU
                                  functions.
            @details              The initialization creates objects to be
                                  used in any function in the IMU class.
            @param i2c            The I2C object to be modified.
        '''
        self.i2c = i2c
        
        filename = "RT_cal_coeffs.txt"
        if filename in os.listdir():
            self.FileFlag = True
        else:
            self.FileFlag = False
        
    def change_op(self, mode):
        ''' @brief              Changes the zero of the IMU.
            @details            Sets a hexadecimal number, to zero the IMU.
        '''
        self.i2c.mem_write(mode, 0x28, 0x3D)
        
    def get_status(self):
        ''' @brief              Get the current calibration status.
            @details            Reports the calibration satus as a string of
                                four numbers. Four threes indicates a fully
                                calibrated IMU.
            @return             The calibration status.
        '''
        cal_bytes = bytearray(1)
        
        self.i2c.mem_read(cal_bytes, 0x28, 0x35) 
        
        cal_status = (cal_bytes[0] & 0b11,
                     (cal_bytes[0] & 0b11 << 2) >> 2,
                     (cal_bytes[0] & 0b11 << 4) >> 4,
                     (cal_bytes[0] & 0b11 << 6) >> 6)

        print("Values:", cal_status)
        print('\n')
        return '{:}'.format(cal_status)
        
    def get_calib(self):
        ''' @brief              Get the current calibration coefficients.
            @details            Reports the calibration coefficients as a
                                byte array.
            @return             The calibration coefficient byte array.
        '''
        calib_bytes = bytearray(22)
        self.i2c.mem_read(calib_bytes, 0x28, 0x55)
        return calib_bytes
        
    def set_calib(self, data):
        ''' @brief              Set the calibration coefficients.
            @details            Writes the calibration coefficients to the
                                IMU.
        '''
        if self.FileFlag == False:
            self.i2c.mem_write(data, 0x28, 0x55)
        
    def get_euler(self):
        ''' @brief              Get the euler angles from the IMU.
            @details            Reads the IMU for the euler angles and returns
                                a tuple of them.
            @return             The euler angles measured by the IMU.
        '''
        eul_bytes = bytearray(6)
        self.i2c.mem_read(eul_bytes, 0x28, 0x1A)
        
        # First, unpack bytes into three signed integers
        eul_signed_ints = struct.unpack('<hhh', eul_bytes)
        
        # Second, scale ints to get proper units
        self.eul_vals = tuple(eul_int/16 for eul_int in eul_signed_ints)
        
        return self.eul_vals
        
    def get_vel(self):
        ''' @brief              Get the angular velocities from the IMU.
            @details            Reads the IMU for the angular velocities and 
                                returns a byte array of them.
            @return             The angular velocities measured by the IMU.
        '''
        vel_bytes = bytearray(6)
        self.i2c.mem_read(vel_bytes, 0x28, 0x14)
        
        # Unpack bytes into three signed integers
        self.vel_signed_ints = struct.unpack('<hhh', vel_bytes)
        
        return self.vel_signed_ints
        
    def report_euler(self):
        ''' @brief              Prints the euler angles to the REPL
        '''
        time.sleep(.2)
        print('Scaled: ', self.eul_vals)
        
    def report_vel(self):
        ''' @brief              Prints the angular velocities to the REPL
        '''
        time.sleep(.2)
        print('Velocities: ', self.vel_signed_ints)
        
    
if __name__ == '__main__':
    i2c = pyb.I2C(1, pyb.I2C.MASTER)
    drv = IMU(i2c)
    drv.change_op(0x0C)
    while True:
        time.sleep(0.5)
        drv.get_status()
        if drv.get_status() == '(3, 3, 3, 3)':
            break
    data = drv.get_calib()
    drv.set_calib(data)
    while True:
        time.sleep(0.5)
        drv.get_euler()
#    while True:
#        time.sleep(0.5)
#        drv.get_vel()
#       i2c.mem_read(4, 0x28, 0x00)
