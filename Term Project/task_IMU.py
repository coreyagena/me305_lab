""" @file        task_IMU.py
    @brief       IMU task for running the IMU.
    @details     Responsible for interfacing with the IMU
                 using an object of the IMU driver class.
    @author      Corey Agena
    @author      Austin Fong
    @date        December 3, 2021
"""
# All imports near the top
import os
from micropython import const
import utime

## State 0 of the user interface task
S0_CALIB            = const(0)
## State 1 of the user interface task
S1_UPDATE           = const(1)

class Task_IMU:
    ''' @brief      IMU task class for the IMU.
        @details    Implements a class that can be used for other applications.
                    IMU Task Finite State Machine
                    \image html Final_Task_IMU_FSM.jpg
    '''
    def __init__(self, period, IMU_drv, i2c, calib_flag, cal_values,
                 state_vect1, state_vect2, dbg = False):
        ''' @brief              Constructs IMU task objects.
            @details            The IMU task is implemented as a finite 
                                state machine.
            @param period       The period used to run the IMU task.
            @param IMU_drv      The IMU driver used for the IMU task.
            @param i2c          Serial communication protocall for the IMU.
            @param calib_flag   The flag to indicate if manual calibration
                                is required.
            @param cal_values   The calibration values for the IMU.
            @param state_vect1  The state vector representing the data in
                                the y-direction.
            @param state_vect2  The state vector representing the data in
                                the x-direction.                       
            @param dbg          A boolean flag used to enable or disable 
                                debug messages printed over the VCP.  
        '''
        ## Create a object to represent the period for the panel task.
        self.period = period
        
        ## Create a panel driver object.
        self.IMU_drv = IMU_drv
        
        ## A shares.Share object for the IMU data.
        self.i2c = i2c
        
        ## A shares.Share flag used to indicate the need for manual
        ## calibration.
        self.calib_flag = calib_flag
        
        ## Set the transition to the first state of the finite state machine.
        self.state = S0_CALIB
        
        ## A shares.Share object to represent the calibration values
        self.cal_values = cal_values
        
        ## A shares.Share object to represent the state vectors.
        self.state_vect1 = state_vect1
        self.state_vect2 = state_vect2
        
        ## Set the next_time variable for the first iteration of the code
        self.next_time = self.period
        
        ## Set the initial time to zero
        self.initial_time = 0
        
        ## The number of runs of the state machine
        self.runs = 0
        
        self.dbg = dbg
    
    def run(self):
        ''' @brief         Runs one iteration of the task.
            @details       Waits for a command and executes based on 
                           the typed letter.
        '''
        current_time = utime.ticks_us()
        if (utime.ticks_diff(current_time, self.initial_time) >=
            self.next_time):
            if self.runs == 0:
                self.initial_time = utime.ticks_us()
        
            elif self.state == S0_CALIB:
                self.IMU_drv.change_op(0x0C)
                filename = "IMU_cal_coeffs.txt"
                if filename in os.listdir():
                    if self.calib_flag != 2:
                        with open(filename, 'r') as f:
                            # Read the first line of the file
                            cal_data_string = f.readline()
                            # Split the line into multiple strings
                            # and then convert each one to a float
                            cal = [int(cal_value) for cal_value in 
                                   cal_data_string.strip().split(',')]
                            self.IMU_drv.set_calib(cal)
                            self.calib_flag.write(1)
                            print('IMU calibration complete with existing'
                                  ' file.')
                            self.transition_to(S1_UPDATE)
                else:
                    # File doesnt exist, calibrate manually and 
                    # write the coefficients to the file
                    print('IMU calibration required.')
                    while True:
                        utime.sleep(0.5)
                        self.IMU_drv.get_status()
                        if self.IMU_drv.get_status() == '(3, 3, 3, 3)':
                            cal_values = self.IMU_drv.get_calib()
                            break
                    self.IMU_drv.set_calib(cal_values)
                    cal_list = [0]*22
                    i = 0
                    for x in cal_values:
                        cal_list[i] = hex(cal_values[i])
                        i += 1
                    with open(filename, 'w') as f:
                        # Perform manual calibration
                        self.cal_values.write(cal_list)
                        # Then, write the calibration coefficients to the
                        # file as a string. The example uses an f-string,
                        # but you can use string.format() if you prefer
                        (R0,R1,R2,R3,R4,R5,R6,R7,R8,R9,R10,R11,R12,R13,R14,R15,R16,R17,R18,R19,R20,R21) = cal_list
                        f.write(f"{R0},{R1},{R2},{R3},{R4},{R5},{R6},{R7},{R8},{R9},{R10},{R11},{R12},{R13},{R14},{R15},{R16},{R17},{R18},{R19},{R20},{R21}\r\n")
                        #f.write(''.join(f'{i},' for i in cal_list))
                        self.calib_flag.write(1)
                        self.transition_to(S1_UPDATE)
            
            elif self.state == S1_UPDATE:
                angle = self.IMU_drv.get_euler()
                angle_vel = self.IMU_drv.get_vel()
                self.state_vect1[1].write(angle[2])
                self.state_vect2[1].write(angle[1])
                self.state_vect1[3].write(angle_vel[1])
                self.state_vect2[3].write(angle_vel[2])
            
            self.next_time += self.period
            self.runs += 1
                
        
    def transition_to(self, new_state):
        ''' @brief      Transitions the FSM to a new state.
            @details    Optionally a debugging message can be printed
                        if the dbg flag is set when the task object is created.
            @param      new_state The state to transition to.
        '''
        if (self.dbg):
            print('{:}: S{:}->S{:}'.format(self.name,self.state,new_state))
        self.state = new_state