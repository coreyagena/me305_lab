""" @file        task_panel.py
    @brief       Panel task for running the touch panel.
    @details     Responsible for interfacing with the resistive touch panel
                 using an object of the touch panel driver class.
    @author      Corey Agena
    @author      Austin Fong
    @date        December 3, 2021
"""
# All imports near the top
import os
from micropython import const
import utime

## State 0 of the user interface task
S0_CALIB            = const(0)
## State 1 of the user interface task
S1_UPDATE           = const(1)

class Task_Panel:
    ''' @brief      Panel class task for the Touch Panel.
        @details    Implements a class that can be used from other files.
                    Task Panel Finite State Machine.
                    \image html Final_Task_Panel_FSM.jpg
    '''
    def __init__(self, period, panel_drv, calib_flag, cal_values,
                 state_vect1, state_vect2, b_flag, dbg = False):
        ''' @brief              Constructs panel task objects.
            @details            The panel task is implemented as a finite 
                                state machine.
            @param period       The period used to run the panel task.
            @param panel_drv    The panel driver used for the panel task.
            @param calib_flag   The flag to indicate if manual calibration is
                                required.
            @param cal_values   Calibration values of the touch panel.
            @param state_vect1  The state vector representing the data in
                                the y-direction.
            @param state_vect2  The state vector representing the data in
                                the x-direction.
            @param b_flag       Flag to indicate the controller state.
            @param dbg          A boolean flag used to enable or disable 
                                debug messages printed over the VCP.  
        '''
        ## Create a object to represent the period for the panel task.
        self.period = period
        
        ## Create a panel driver object.
        self.panel_drv = panel_drv
        
        ## A shares.Share flag used to indicate the need for manual
        ## calibration.
        self.calib_flag = calib_flag

        ## shares.Share objects to represent the state vectors.
        self.state_vect1 = state_vect1
        self.state_vect2 = state_vect2
        
        ## A shares.Share object to represent if the controller should be ran
        self.b_flag = b_flag
        
        ## Set the transition to the first state of the finite state machine.
        self.state = S0_CALIB
        
        ## A shares.Share object to represent the calibration values
        self.cal_values = cal_values
        
        ## Set the next_time variable for the first iteration of the code
        self.next_time = self.period
        
        ## Set the initial time to zero
        self.initial_time = 0
        
        ## The number of runs of the state machine
        self.runs = 0
        
        self.dbg = dbg
    
    def run(self):
        ''' @brief         Runs one iteration of the task.
            @details       Waits for a command and executes based on 
                           the typed letter.
        '''
        current_time = utime.ticks_us()
        if (utime.ticks_diff(current_time, self.initial_time) >=
            self.next_time):
            if self.runs == 0:
                self.initial_time = utime.ticks_us()
        
            elif self.state == S0_CALIB:
                filename = "RT_cal_coeffs.txt"
                if filename in os.listdir():
                    if self.calib_flag.read() != 1:
                        with open(filename, 'r') as f:
                            # Read the first line of the file
                            cal_data_string = f.readline()
                            # Split the line into multiple strings
                            # and then convert each one to a float
                            cal = [float(cal_value) for cal_value in 
                                   cal_data_string.strip().split(',')]
                            self.cal_values.write(cal)
                            self.calib_flag.write(1)
                            print('Touch Panel calibration complete with '
                                  'existing file.')
                            self.transition_to(S1_UPDATE)
                else:
                    # File doesnt exist, calibrate manually and 
                    # write the coefficients to the file
                    print('Touch Panel calibration required.')
                    cal_values = self.panel_drv.calibrate()
                    # self.calib_flag.write(1)
                    # if self.calib_flag.read() == 1:
                    with open(filename, 'w') as f:
                        # Perform manual calibration
                        self.cal_values.write(cal_values)
                        Kxx = cal_values[0]
                        Kxy = cal_values[1]
                        Kyx = cal_values[2]
                        Kyy = cal_values[3]
                        Xc = cal_values[4]
                        Yc = cal_values[5]
                        # Then, write the calibration coefficients to the
                        # file as a string. The example uses an f-string,
                        # but you can use string.format() if you prefer
                        f.write(f"{Kxx}, {Kxy}, {Kyx}, {Kyy}, {Xc}, {Yc}\r\n")
                        self.calib_flag.write(1)
                        self.transition_to(S1_UPDATE)
            
            elif self.state == S1_UPDATE:
                xy_coords = self.panel_drv.update()
                if self.b_flag.read() == 1:
                    pos_current = [0,0]
                    vel_current = [0,0]
                    self.state_vect1[0].write(pos_current[0])
                    self.state_vect2[0].write(pos_current[1])
                    self.state_vect1[2].write(vel_current[0])
                    self.state_vect2[2].write(vel_current[1])
                elif self.b_flag.read() == 2:
                    pos_current = (xy_coords[0],xy_coords[1])
                    vel_current = (xy_coords[2],xy_coords[3])
                    self.state_vect1[0].write(pos_current[0])
                    self.state_vect2[0].write(pos_current[1])
                    self.state_vect1[2].write(vel_current[0])
                    self.state_vect2[2].write(vel_current[1])
            
            self.next_time += self.period
            self.runs += 1
                
        
    def transition_to(self, new_state):
        ''' @brief      Transitions the FSM to a new state.
            @details    Optionally a debugging message can be printed
                        if the dbg flag is set when the task object is created.
            @param      new_state The state to transition to.
        '''
        if (self.dbg):
            print('{:}: S{:}->S{:}'.format(self.name,self.state,new_state))
        self.state = new_state
    