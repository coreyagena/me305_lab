""" @file        task_motor.py
    @brief       Motor task for running the motor.
    @details     Implements a finite state machine to run the motor at the
                 duty cycle provided by the user or the controller.
                 
                 Lab 3 task motor finite state machine.
                 \image html Lab3_Task_Motor_FSM.jpg
                 
                 Lab 4 task motor finite state machine.
                 \image html Lab4_Task_Motor_FSM.jpg
                 
    @author      Corey Agena
    @author      Austin Fong
    @date        October 24, 2021
"""
import utime

class Task_Motor:
    ''' @brief      Motor task class for running the motor.
        @details    Runs the motor at the provided duty cycle
    '''
    def __init__(self, period, motor_drv, duty, mot, cont, FaultFlag,
                 ContFlag):    
        ''' @brief              Constructs motor task objects.
            @details            The motor task is implemented as a finite 
                                state machine.
            @param period       The period used to run the motor task.
            @param motor_drv    The motor driver of the system.
            @param duty         The duty cycle of the motor.
            @param mot          The motor object that is affected.
            @param cont         The controller object.
            @param FaultFlag    Flag to indicate if the fault condition is
                                active or not.
            @param ContFlag     Flag to indicate if the controller should be
                                updated.
        '''
        ## The period (in us) of the task
        self.period = period
        
        ## Create a motor driver object
        self.motor_drv = motor_drv       
        
        ## The intended duty cycle of the motor
        self.duty = duty
        
        ## The motor object that will be affected.
        self.mot = mot
        
        ## The controller object that will be affected
        self.cont = cont
        
        ## The flag that will stop the motor if a fault is detected.
        self.FaultFlag = FaultFlag
        
        ## The flag that will allow the controller to run.
        self.ContFlag = ContFlag
        
        ## Set the next_time variable for the first iteration of the code
        self.next_time = self.period
        
        ## Set the initial time to zero
        self.initial_time = 0
        
        ## The number of runs of the state machine
        self.runs = 0
    
    def run(self):
        ''' @brief         Runs one iteration of the task.
            @details       It sets the duty cycle of the motor.
        '''
        current_time = utime.ticks_us()
        if (utime.ticks_diff(current_time, self.initial_time) >=
            self.next_time):
            if self.runs == 0:
                self.initial_time = utime.ticks_us()
            elif self.FaultFlag.read() == False:
                if self.duty != 0 and self.ContFlag.read() == False:
                    self.mot.set_duty(self.duty.read())
                elif self.duty != 0 and self.ContFlag.read() == True:
                    self.cont.update()
                    self.mot.set_duty(self.duty.read())
            else:
                self.mot.set_duty(0)
            self.next_time += self.period
            self.runs += 1