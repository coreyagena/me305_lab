''' @file                controller_page.py
    @brief               File that holds the controller documentation.
    @details             The controller documentation includes the plot
                         progression for tuning the proportional gain, a block
                         diagram, and details for the three tasks.
    
    @page page1          Controller Documentation
    
    @section sec_plots   Plots
                         This section shows the progression of Plots for lab 4.
                         The proportional gain was increased until it the
                         controller became unstable.
                         
                         Initially, the data did not look correct because a
                         full plot of the 10 seconds of data was created. Once
                         the time axis was reduced to 1 second then the
                         transient portion of the data was much clearer. Still, 
                         the transient portion was small because the motor
                         got up to speed within two or three periods of the
                         controller driver, which was set to 20 ms.
                         
                         The first plot was created with a proportional gain
                         of 0.5 (%-s)/rad and a set point of 150 rad/s.
                         Throughout the progression the set point remained
                         constant while the proportional gain increased by
                         0.5 (%-s)/rad. Each time the proportional gain
                         increased the steady state angular velocity would
                         increase toward the desired angular velocity of
                         150 rad/s. This pattern indicated that the best
                         proportional gain would be the highest value that
                         remained a stable system.
                         
                         \image html Lab4_Kp0.5.png
                         
                         \image html Lab4_Kp1.0.png
                         
                         \image html Lab4_Kp1.5.png
                         
                         \image html Lab4_Kp2.0.png
                         
                         \image html Lab4_Kp2.5.png
                         
                         \image html Lab4_Kp3.0.png
                         
                         When a Kp of 3.0 (%-s)/rad was applied the plot
                         became steady near 150 rad/s. From that point the Kp
                         value was increased by 0.25 (%-s)/rad.
                         
                         \image html Lab4_Kp3.25.png
                         
                         The plot of Kp = 3.25 (%-s)/rad shows that the
                         controller becomes unstable and maximizes the
                         actuation value after approximately 1 second. The Kp
                         was reduced to 3.1 (%-s)/rad.
                         
                         \image html Lab4_Kp3.1.png
                         
                         This plot of Kp = 3.1 (%-s)/rad became unstable after
                         approximately 4 seconds. the Kp of 3.0 (%-s)/rad was
                         verified to make sure that it did not become unstable
                         with the plot below.
                         
                         \image html Lab4_Kp3.0L.png
                         
                         The plot above shows that the Kp = 3.0 (%-s)/rad 
                         stayed stable through 10 seconds. This proportional
                         gain is the best for the set point of 150 rad/s.

    @section sec_diagram Closed Loop Block Diagram
                         The block diagram below shows the proportional gain
                         controller implemented to control the angular
                         velocity of the motor. The input being the reference
                         angular velocity.
                         
                         \image html Lab4_Block_Diagram.jpg
                         
    @section sec_tasks   Tasks
                         Three tasks are run during the implementation of the
                         controller. Every task is ran through the main file, 
                         main.py. The tasks include: encoder, motor, and user
                         tasks.
                         
                         All three tasks and their interactions with shares
                         are represented by the task diagram below. The dashed
                         lines represent shares between the tasks.
                         
                         \image html Lab4_Task_Diagram.jpg
                         
                         The encoder task continuosly updates the encoder
                         position and zeros the encoder when required. This
                         task was not altered by the addition of the
                         controller.
                         
                         \image html Lab2_Task_Encoder_FSM.PNG
                         
                         The motor task runs the motor 1 or 2 at the duty
                         cycle provided by the user or the controller. The
                         finite state machine was altered when the controller
                         was added. A controller flag was created. When it is
                         false the task reads the duty cycle provided by the
                         user and writes it to the motor. When the controller
                         flag is true the controller is updated then the
                         actuation output is written to the motor duty cycle.
                         The task finite state machine is shown in the figure
                         below.
                         
                         \image html Lab4_Task_Motor_FSM.jpg
                         
                         Finally, the user task allows the user to interact
                         with the encoders, motors, and controllers. The task
                         continuously grows as more commands are added to it.
                         The addition of the controller created four new
                         states. State 5 accepts the initial proportional gain
                         value. State 6 accepts the set point angular velocity.
                         State 7 initiates the and collects the data in arrays.
                         Finally, state 8 prints the data in the putty window.
                         
                         \image html Lab4_Task_User_FSM.jpg
'''