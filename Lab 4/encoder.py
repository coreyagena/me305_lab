""" @file        encoder.py
    @brief       A driver for reading from Quadrature Encoders.
    @details     The driver updates continuously and resets to zero when
                 required.
    @author      Corey Agena
    @author      Austin Fong
    @date        October 5, 2021
"""
## All imports near the top
import pyb

class Encoder:
    ''' @brief             Interface with quadrature encoders
        @details           The Encoder class can be called and used with
                           different encoders outside of lab 2.
    '''
    def __init__(self, enc_pin1, enc_pin2, enc_tim):
        ''' @brief                Constructs an encoder object
            @details              The initialization creates the pin objects
                                  and channels so that the code interfaces
                                  with the hardware.
            @param enc_pin1       A string representing the first pin
                                  relevant to the encoder on the nucleo.
            @param enc_pin2       A string representing the second pin
                                  relevant to the encoder on the nucleo.
            @param enc_tim        Integer that represents the timer relevant
                                  to the encoder on the nucleo.
        '''    
        self.pin1 = pyb.Pin(enc_pin1,pyb.Pin.OUT_PP)
        self.pin2 = pyb.Pin(enc_pin2,pyb.Pin.OUT_PP)
        
        self.tim = pyb.Timer(enc_tim, prescaler = 0, period = 65535)
        self.tch1 = self.tim.channel(1, pyb.Timer.ENC_AB, pin = self.pin1)
        self.tch2 = self.tim.channel(2, pyb.Timer.ENC_AB, pin = self.pin2)
        
        self.pos = 0
        self.past = 0
        
    def update(self):
        ''' @brief          Finds encoder position.
            @details        Position is found without encoder overflow.
        '''
        self.current = self.tim.counter()
        self.dlt = self.current - self.past
        if self.dlt < -32768:
            self.dlt += 65536
        elif self.dlt > 32768:
            self.dlt -= 65536
        self.pos += self.dlt
        self.past = self.current
            
    def get_position(self):
        '''@brief           Gets the encoder's position.
           @details         Calls the value from update() and returns it.
           @return          The position of the encoder.
        '''
        return self.pos

    def set_position(self, position):
        '''@brief           Replaces the position of the encoder.
           @details         The position is set to zero and the self.past
                            variable is reset for a smooth transition.
           @param position  The new position value to assign to the encoder's
                            present location.
        '''
        self.pos = position
        self.past = self.tim.counter()
    
    def get_delta(self):
        ''' @brief          Returns encoder delta.
            @details        Calls the value from update() and returns it.
            @return         The change in position of the encoder shaft
                            between the two most recent updates.
        '''
        return self.dlt
        